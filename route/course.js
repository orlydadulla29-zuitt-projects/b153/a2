//setup the dependencies
const express = require("express")
const router = express.Router()
//import the controller file
const courseController = require("../controllers/course");
const auth = require("../auth");

//route to get all courses
router.get("/", (req, res) => {
	courseController.getCourses().then(resultFromController => res.send(resultFromController))
})


//route to get specific course

router.get("/:courseId", (req, res) => {
	//" /:courseId" here is called a "wildcard" and its value is anything that is added at the end of localhost:4000/courses

	//e.g. localhost:4000/courses/dog = the value of our wildcard is "dog"

	// console.log(req.params)

	courseController.getSpecific(req.params.courseId).then(resultFromController => res.send(resultFromController))
	
})

//route to create a new course

//when a user sends a specific method to a specific endpoint (in this case a POST request to our /courses endpoint) the code within this route will be run
router.post("/", auth.verify,(req, res) => {
	//auth.verify here is something called "middleware"

	//show in the console the request body
	// console.log(req.body)

	//invoke the addCourse method contained in the courseController module, which is an object. Pages, when imported via Node are treated as objects.

	//We also pass req.body to addCourse as part of the data that it needs

	//Once addCourse has resolved, .then can send whatever it returns (true or false) as its response

/*	console.log(auth.decode(req.headers.authorization))

	res.send("Hello")*/
	
	if(auth.decode(req.headers.authorization).isAdmin){

	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send("auth: failed")
	}
})

//route to update a single course

router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))
})


/*
Activity:

Create a route and a controller for disabling/archiving a course with the ff. specifications:

1. Route must use a DELETE request
2. Include middleware to verify the user's token
3. Restrict access to this route to ONLY admin users
4. Name the controller function "archiveCourse"
5. Return true if successful, false if not.

HINT: How do we consider courses to be inactive?

When finished, copy the course route and course controller files to a2, and push a2 to Gitlab, then paste the link to Boodle.

Please type "done" in the chat when finished.

*/

//route to delete/archive a single course

router.delete("/:courseId", auth.verify, (req, res) => {
	
	if(auth.decode(req.headers.authorization).isAdmin){

	courseController.archiveCourse(req.params.courseId, req.body).then(resultFromController => res.send(resultFromController))
	}else{
		return res.send("auth: failed")
	}
})


//export the router
module.exports = router;